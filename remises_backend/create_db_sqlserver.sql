USE master
IF EXISTS(select * from sys.databases where name='fepsa')
DROP DATABASE fepsa

CREATE DATABASE fepsa

USE [fepsa]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[choferes](
	[id] [int] NOT NULL IDENTITY(1,1),
	[nombre_apellido] [nchar](10) NOT NULL,
	[dni] [nchar](10) NOT NULL,
	[id_remisera] [int] NOT NULL,
	[vigente] [int] NOT NULL,
 CONSTRAINT [PK_choferes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[parametros_logica](
	[id] [int] NOT NULL IDENTITY(1,1),
	[on_off_rumbo] [int] NOT NULL,
	[on_off_tiempo_velocidad] [int] NOT NULL,
	[on_off_distancia_velocidad] [int] NOT NULL,
	[velocidad_v1] [decimal](18,0) NOT NULL,
	[velocidad_v2] [decimal](18,0) NOT NULL,
	[velocidad_v3] [decimal](18,0) NOT NULL,
	[velocidad_v4] [decimal](18,0) NOT NULL,
	[tiempo_t1] [decimal](18,0) NOT NULL,
	[tiempo_t2] [decimal](18,0) NOT NULL,
	[tiempo_t3] [decimal](18,0) NOT NULL,
	[distancia_x1] [decimal](18,0) NOT NULL,
	[distancia_x2] [decimal](18,0) NOT NULL,
	[distancia_x3] [decimal](18,0) NOT NULL,
	[rumbo_grados_rg] [decimal](18,0) NOT NULL,
	[rumbo_distancia_rd] [decimal](18,0) NOT NULL,
	[version] [int] NOT NULL
 CONSTRAINT [PK_parametros_logica] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pasajeros_fepsa](
	[id] [int] NOT NULL IDENTITY(1,1),
	[legajo] [nchar](10) NOT NULL,
	[nombre_apellido] [nchar](10) NOT NULL,
 CONSTRAINT [PK_pasajeros_fepsa] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pasajeros_viajes](
	[id_viaje] [int] NOT NULL,
	[id_pasajero] [int] NULL,
	[nombre_apellido] [nchar](10) NOT NULL,
	[id_posicion_sube] [int] NOT NULL,
	[id_posicion_baja] [int] NOT NULL,
	[id] [int] NOT NULL IDENTITY(1,1),
 CONSTRAINT [PK_pasajeros_viajes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[posicion](
	[id] [bigint] NOT NULL IDENTITY(1,1),
	[id_viaje] [int] NOT NULL,
	[fecha] [datetime] NOT NULL,
	[latitud] [int] NOT NULL,
	[longitud] [int] NOT NULL,
	[rumbo] [nchar](10) NOT NULL,
	[velocidad] [nchar](10) NOT NULL,
	[observacion] [nchar](10) NULL,
	[fecha_insercion] [datetime] NOT NULL,
	[id_version_logica] [int] NULL,
 CONSTRAINT [PK_posicion] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[remiseras](
	[id] [int] NOT NULL IDENTITY(1,1),
	[codigo_activacion] [nchar](10) NOT NULL,
	[vigente] [int] NOT NULL,
 CONSTRAINT [PK_remiseras] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[versiones](
	[id] [int] NOT NULL IDENTITY(1,1),
	[nro_lista_pasajeros] [int] NOT NULL,
	[nro_lista_parametros] [int] NOT NULL,
 CONSTRAINT [PK_versiones] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[viajes](
	[id] [int] NOT NULL IDENTITY(1,1),
	[id_chofer] [int] NOT NULL,
	[fecha_inicio] [datetime] NOT NULL,
	[fecha_fin] [datetime] NOT NULL,
	[t_espera] [int] NOT NULL,
	[peajes] [int] NOT NULL,
 CONSTRAINT [PK_viajes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[choferes]  WITH CHECK ADD  CONSTRAINT [FK_choferes_remiseras] FOREIGN KEY([id_remisera])
REFERENCES [dbo].[remiseras] ([id])
GO
ALTER TABLE [dbo].[choferes] CHECK CONSTRAINT [FK_choferes_remiseras]
GO
ALTER TABLE [dbo].[pasajeros_viajes]  WITH CHECK ADD  CONSTRAINT [FK_pasajeros_viajes_pasajeros_fepsa] FOREIGN KEY([id_pasajero])
REFERENCES [dbo].[pasajeros_fepsa] ([id])
GO
ALTER TABLE [dbo].[pasajeros_viajes] CHECK CONSTRAINT [FK_pasajeros_viajes_pasajeros_fepsa]
GO
ALTER TABLE [dbo].[pasajeros_viajes]  WITH CHECK ADD  CONSTRAINT [FK_pasajeros_viajes_viajes] FOREIGN KEY([id_viaje])
REFERENCES [dbo].[viajes] ([id])
GO
ALTER TABLE [dbo].[pasajeros_viajes] CHECK CONSTRAINT [FK_pasajeros_viajes_viajes]
GO
ALTER TABLE [dbo].[posicion]  WITH CHECK ADD  CONSTRAINT [FK_posicion_viajes] FOREIGN KEY([id_viaje])
REFERENCES [dbo].[viajes] ([id])
GO
ALTER TABLE [dbo].[posicion] CHECK CONSTRAINT [FK_posicion_viajes]
GO
ALTER TABLE [dbo].[viajes]  WITH CHECK ADD  CONSTRAINT [FK_viajes_choferes] FOREIGN KEY([id_chofer])
REFERENCES [dbo].[choferes] ([id])
GO
ALTER TABLE [dbo].[viajes] CHECK CONSTRAINT [FK_viajes_choferes]
GO
