﻿using System;
using System.Configuration;

using NPoco;

using Nancy;
using Nancy.Diagnostics;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;
using Nancy.Conventions;
using AutoMapper;
using Newtonsoft.Json;
using remises_backend.AccesoDatos.Repositorios;
using remises_backend.AccesoDatos.Tablas;
using remises_backend.Infraestructura.Repositorios;

namespace remises_backend.Infraestructura
{
    using AccesoDatos;
    using Infraestructura.Mappers;
    
    public class Bootstrapper : Nancy.DefaultNancyBootstrapper
    {
        protected override void ApplicationStartup(TinyIoCContainer container, Nancy.Bootstrapper.IPipelines pipelines)
        {
            base.ApplicationStartup(container, pipelines);

            StaticConfiguration.DisableErrorTraces = false;

            LogAllRequests(pipelines);
            LogAllResponseCodes(pipelines);
            LogUnhandledExceptions(pipelines);

            MyFactory.Setup();
        }
        
        protected override void ConfigureConventions(NancyConventions nancyConventions)
        {
            base.ConfigureConventions(nancyConventions);
            nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("", @"Content"));
        }
        
        #region Logging
        private void LogAllRequests(IPipelines pipelines)
        {
            pipelines.BeforeRequest += ctx =>
            {
                System.Console.WriteLine("Handling request {0} \"{1}\"", ctx.Request.Method, ctx.Request.Path);
                return null;
            };
        }

        private void LogAllResponseCodes(IPipelines pipelines)
        {
            pipelines.AfterRequest += ctx =>
                System.Console.WriteLine("Responding {0} to {1} \"{2}\"", ctx.Response.StatusCode, ctx.Request.Method, ctx.Request.Path);
            
            //CORS Enable
            pipelines.AfterRequest.AddItemToEndOfPipeline((ctx) =>
            {
                ctx.Response.WithHeader("Access-Control-Allow-Origin", "*")
                    .WithHeader("Access-Control-Allow-Methods", "POST,GET")
                    .WithHeader("Access-Control-Allow-Headers", "Accept, Origin, Content-type");

            });
        }

        private void LogUnhandledExceptions(IPipelines pipelines)
        {
            pipelines.OnError.AddItemToStartOfPipeline((ctx, err) =>
            {
                System.Console.WriteLine(string.Format("Request {0} \"{1}\" failed because {2}", ctx.Request.Method, 
                                                       ctx.Request.Path, err.Message));
                return null;
            });
        }
        #endregion

        protected override void ConfigureApplicationContainer(TinyIoCContainer container)
        {
            base.ConfigureApplicationContainer(container);
            container.Register<JsonSerializer, CustomJsonSerializer>();
        }

        protected override void ConfigureRequestContainer(TinyIoCContainer container, Nancy.NancyContext context)
        {
            base.ConfigureRequestContainer(container, context);

            container.Register<IDatabase>(MyFactory.DbFactory.GetDatabase());
            container.Register<IRepositorioChoferes, NPocoRepositorioChoferes>();
            container.Register<IRepositorioPasajeros, NPocoRepositorioPasajeros>();
            container.Register<IRepositorio<Remisera>, NPocoRepositorio<Remisera>>();
            container.Register<IRepositorioPosiciones, NPocoRepositorioPosiciones>();
            container.Register<IRepositorioViajes, NPocoRepositorioViajes>();
            container.Register<IRepositorio<VersionPasajero>, NPocoRepositorio<VersionPasajero>>();
            container.Register<IRepositorio<ParametroLogica>, NPocoRepositorio<ParametroLogica>>();
            container.Register<IRepositorio<PasajeroFepsa>, NPocoRepositorio<PasajeroFepsa>>();
            container.Register<IRepositorioVersionPasajero, NPocoRepositorioVersionPasajero>();
            container.Register<IRepositorioRemiseras, NPocoRepositorioRemiseras>();
            container.Register(typeof(AutoMapper.IMapper), MapperFactory.GetInstance());
        }
    }
}
