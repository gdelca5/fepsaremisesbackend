﻿using System.Collections.Generic;
using remises_backend.AccesoDatos.Tablas;

namespace remises_backend.Infraestructura.Repositorios
{
    public interface IRepositorioPosiciones : IRepositorio<Posicion>
    {
        List<Posicion> guardarPosiciones(List<Posicion> posiciones);
    }
}