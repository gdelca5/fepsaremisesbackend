﻿using System.Collections.Generic;
using remises_backend.AccesoDatos.Tablas;

namespace remises_backend.Infraestructura.Repositorios
{
    public interface IRepositorioPasajeros : IRepositorio<PasajeroViaje>
    {
        void guardarPasajeros(List<PasajeroViaje> pasajeros);
    }
}