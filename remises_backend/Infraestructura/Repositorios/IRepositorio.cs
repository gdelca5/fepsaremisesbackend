﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nancy;
using Nancy.Routing.Constraints;
using remises_backend.AccesoDatos.Tablas;
using remises_backend.Infraestructura.Excepciones;

namespace remises_backend.Infraestructura.Repositorios
{
    public interface IRepositorio<T> where T : Entidad
    {
        List<T> ObtenerTodos();
        int Count();
        void Guardar(T entidad);
    }       
}
