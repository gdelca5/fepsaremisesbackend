﻿using Newtonsoft.Json;
using System;
namespace remises_backend.Infraestructura.Dtos
{
    public class ActivacionChoferGet
    {
        public String chofer_id { get; set; }
        public String nombre { get; set; }
        public String token { get; set; }

        public ActivacionChoferGet() { }
    }

    public class ActivarChoferPost
    {
        [JsonProperty(Required = Required.AllowNull)]
        public string nombre { get; set; }
        [JsonProperty(Required = Required.AllowNull)]
        public string clave { get; set; }
        [JsonProperty(Required = Required.AllowNull)]
        public string dni { get; set; }
    }
}
