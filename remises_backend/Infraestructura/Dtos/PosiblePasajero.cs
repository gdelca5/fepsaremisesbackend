﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace remises_backend.Infraestructura.Dtos
{

    public class PosiblePasajeroInner
    {
        public string pasajeroId { get; set; }
        public string nombre { get; set;  }
        public string legajo { get; set;  }
    }
    
    public class PosiblePasajero
    {
        public List<PosiblePasajeroInner> pasajeros { get; set; }
        public long version { get; set; }
        public PosiblePasajero() { }
    }
}
