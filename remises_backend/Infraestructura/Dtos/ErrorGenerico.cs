﻿using System;
namespace remises_backend.Infraestructura.Dtos
{
    public class ErrorGenerico
    {
        public readonly string error;

        public ErrorGenerico(String error)
        {
            this.error = error;
        }
    }
}
