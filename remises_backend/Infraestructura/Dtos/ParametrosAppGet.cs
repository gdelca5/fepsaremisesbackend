﻿using System;

namespace remises_backend.Infraestructura.Dtos
{
    public class ParametrosAppGet
    {
        public ParametrosAppGet() { }

        public bool controlRumbo { get; set; }
        public bool controlTiempoVelocidad { get; set; }
        public bool controlDistanciaVelocidad { get; set; }

        public decimal velocidad1 { get; set; }
        public decimal velocidad2 { get; set; }
        public decimal velocidad3 { get; set; }
        public decimal Velocidad4 { get; set; }

        public decimal tiempo1 { get; set; }
        public decimal tiempo2 { get; set; }
        public decimal tiempo3 { get; set; }

        public decimal distancia1 { get; set; }
        public decimal distancia2 { get; set; }
        public decimal distancia3 { get; set; }

        public decimal rumboGrados { get; set; }
        public decimal rumboDistancia { get; set; }

        public decimal gpsInterval { get; set; }

        public long versionPasajeros { get; set; }
        public long versionParametros { get; set; }
    }
    
    
    public class ParametrosAppPost
    {
        public ParametrosAppPost() { }

        public bool controlRumbo { get; set; }
        public bool controlTiempoVelocidad { get; set; }
        public bool controlDistanciaVelocidad { get; set; }

        public decimal velocidad1 { get; set; }
        public decimal velocidad2 { get; set; }
        public decimal velocidad3 { get; set; }
        public decimal velocidad4 { get; set; }

        public decimal tiempo1 { get; set; }
        public decimal tiempo2 { get; set; }
        public decimal tiempo3 { get; set; }

        public decimal distancia1 { get; set; }
        public decimal distancia2 { get; set; }
        public decimal distancia3 { get; set; }

        public decimal rumboGrados { get; set; }
        public decimal rumboDistancia { get; set; }
        public decimal gpsInterval { get; set; }
    }
}
