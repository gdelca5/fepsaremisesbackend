﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using Newtonsoft.Json;
using NPoco;
using AutoMapper;
using Nancy;
using remises_backend.AccesoDatos.Tablas;
using remises_backend.Infraestructura.Excepciones;
using remises_backend.Infraestructura.Repositorios;
using System.Linq;

namespace remises_backend.Infraestructura.Modulos
{
    using Infraestructura.Dtos;

    public class ModuloPosiblesPasajeros : Nancy.NancyModule
    {
        public ModuloPosiblesPasajeros(IRepositorio<PasajeroFepsa> repoPasajerosFepsa, AutoMapper.IMapper mapper) :
            base()
        {
            Get[Rutas.PosiblesPasajeros] = parameter =>
            {
                var lista = repoPasajerosFepsa.ObtenerTodos();

                var dtoInner = lista.Select(x =>
                {
                    return mapper.Map<PasajeroFepsa, PosiblePasajeroInner>(x);
                }).ToList();

                var dto = new PosiblePasajero()
                {
                    pasajeros = dtoInner,
                    version = lista.Count == 0 ? -1L : lista[0].IdVersion
                };

                return Response.AsJson<PosiblePasajero>(dto, HttpStatusCode.OK);
            };
        }

        private class SinPasajerosCargadosExcepcion : ExcepcionBackend
        {        
            public SinPasajerosCargadosExcepcion() :
                base("No se encontrarón pasajeros cargados en la base de datos", HttpStatusCode.InternalServerError) { }
        }

    }
}
