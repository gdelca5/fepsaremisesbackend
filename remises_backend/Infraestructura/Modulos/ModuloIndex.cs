﻿using Nancy;

namespace remises_backend.Infraestructura.Modulos
{
    public class IndexModule : NancyModule
    {
        public IndexModule() : base("")
        {
            Get["/tablas"] = parameters => Response.AsFile("Content/index.html", "text/html");
            Get["/"] = parameters => Response.AsFile("Content/index2.html", "text/html");
        }
    }
}