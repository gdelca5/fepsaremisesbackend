﻿using System;
using Microsoft.CSharp.RuntimeBinder;
using Nancy;
using Nancy.Extensions;
using Newtonsoft.Json;
using NPoco;
using remises_backend.AccesoDatos.Tablas;
using remises_backend.Infraestructura.Dtos;
using remises_backend.Infraestructura.Excepciones;
using remises_backend.Infraestructura.Modulos;
using remises_backend.Infraestructura.Repositorios;
using IMapper = AutoMapper.IMapper;

namespace remises_backend.Modulos
{
    public class ModuloActivarChofer : NancyModule
    {
        public ModuloActivarChofer(IRepositorioChoferes repo,
                                   IRepositorioRemiseras repoRemiseras,
                                   IMapper mapper) : base()
        {                    
            Post[Rutas.ActivarChofer] = _ =>
            {
                var cuerpo = deserializarCuerpo();
               
                var chofer = repo.buscarChofer(cuerpo.dni, cuerpo.clave);

                if (chofer == null)
                {
                    var remisera = repoRemiseras.buscarRemisera(cuerpo.clave);

                    if (remisera == null)
                        throw new RemiseraNoExiste();

                    if (cuerpo.nombre == null)
                        throw new NombreChoferVacio();

                    if (cuerpo.dni == null)
                        throw new DniChoferVacio();

                    chofer = new Chofer()
                    {
                        NombreApellido = cuerpo.nombre,
                        DNI = cuerpo.dni,
                        IdRemisera = remisera.Id,
                        Vigente = true
                    };

                    repo.guardarChofer(chofer);
                }

                var dto = mapper.Map<Chofer, ActivacionChoferGet>(chofer);

                return Response.AsJson(dto, statusCode: HttpStatusCode.OK);
            };
        }

        private class NombreChoferVacio : ExcepcionBackend
        {
            public NombreChoferVacio() :
                base("El nombre del chofer no puede ser vacio!", HttpStatusCode.BadRequest)
            { }
        }

        private class DniChoferVacio : ExcepcionBackend
        {
            public DniChoferVacio() :
                base("El dni del chofer no puede ser vacio!", HttpStatusCode.BadRequest)
            { }
        }

        private class RemiseraNoExiste : ExcepcionBackend
        {
            public RemiseraNoExiste() :
                base("El código de la remisera es incorrecto o no se encuentra en la base de datos!", HttpStatusCode.BadRequest)
            { }
        }

        private class ParametroNoEncontradoException : ExcepcionBackend
        {
            public ParametroNoEncontradoException() : 
                base("No se recibieron los parametros, por favor vuelva a ver el swagger", HttpStatusCode.BadRequest) {}
        }
        
        private ActivarChoferPost deserializarCuerpo()
        {
            ActivarChoferPost dto = null;
            try
            {
                dto = JsonConvert.DeserializeObject<ActivarChoferPost>(Request.Body.AsString());
            }
            catch (Exception)
            {
                throw new ParametroNoEncontradoException();
            }
            return dto;
        }
    }
}
