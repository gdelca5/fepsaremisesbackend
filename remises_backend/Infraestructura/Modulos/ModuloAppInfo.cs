﻿using System;
using Nancy;
using NPoco;
using NPoco.Linq;
using Newtonsoft.Json;
using AutoMapper;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Nancy.Extensions;
using IMapper = AutoMapper.IMapper;

namespace remises_backend.Infraestructura.Modulos
{
    using AccesoDatos.Tablas;
    using Infraestructura.Dtos;
    using Infraestructura.Repositorios;
    using Infraestructura.Excepciones;

    public class ModuloAppInfo : NancyModule
    {
        public ModuloAppInfo(IRepositorio<ParametroLogica> repo,
            IRepositorioVersionPasajero repoVersion,
            AutoMapper.IMapper mapper) : base()
        {
            Get[Rutas.AppInfoBase] = parameters => obtenerUltimaVersionDisponiblerParametros(repo, repoVersion, mapper);
            Post[Rutas.AppInfoBase] = parameters => insertarNuevaVersionParametros(repo, repoVersion, mapper);
        }

        private string insertarNuevaVersionParametros(IRepositorio<ParametroLogica> repo,
            IRepositorioVersionPasajero repoVersion,
            IMapper mapper)
        {
            var cuerpo = deserializarCuerpo();
            
            var registro = mapper.Map<ParametrosAppPost, ParametroLogica>(cuerpo);

            repo.Guardar(registro);

            return "{}";
        }

        public Nancy.Response obtenerUltimaVersionDisponiblerParametros(IRepositorio<ParametroLogica> repo,
            IRepositorioVersionPasajero repoVersion,
            AutoMapper.IMapper mapper)
        {
            List<ParametroLogica> parametros = repo.ObtenerTodos();

            var versionListaPasajeros = repoVersion.obtenerUltimaVersion();

            if (parametros.Count == 0)
            {
                throw new SinParametrosDeConfiguracionCargadosExcepcion();
            }
            else
            {
                var version = versionListaPasajeros?.Id ?? -1;
                var dto = mapper.Map<ParametroLogica, ParametrosAppGet>(parametros[0], opt =>
                    opt.AfterMap((src, dest) => dest.versionPasajeros = version));

                return Response.AsJson<ParametrosAppGet>(dto, HttpStatusCode.OK);
            }
        }
        
        private ParametrosAppPost deserializarCuerpo()
        {
            ParametrosAppPost dto = null;
            try
            {
                dto = JsonConvert.DeserializeObject<ParametrosAppPost>(Request.Body.AsString());
            }
            catch (Exception)
            {
                throw new ErrorConLosParametrosDeEntrada();
            }
            return dto;
        }
    }

    public class ErrorConLosParametrosDeEntrada : ExcepcionBackend
    {
        public ErrorConLosParametrosDeEntrada() :
            base("Los parametros enviados no estan bien formados!", HttpStatusCode.BadRequest)
        {
        }
    }

    public class SinParametrosDeConfiguracionCargadosExcepcion : ExcepcionBackend
    {
        public SinParametrosDeConfiguracionCargadosExcepcion() :
            base("No se encontrarón parametros de configuración cargados en la base de datos",
                HttpStatusCode.InternalServerError)
        {
        }
    }
}
