﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using AutoMapper;
using Nancy;
using Nancy.Extensions;
using Nancy.ModelBinding;
using Nancy.Routing.Constraints;
using Newtonsoft.Json;
using NPoco;
using NPoco.fastJSON;
using remises_backend.AccesoDatos.Tablas;
using remises_backend.Infraestructura.Dtos;
using remises_backend.Infraestructura.Excepciones;
using remises_backend.Infraestructura.Modulos;
using remises_backend.Infraestructura.Repositorios;
using IMapper = AutoMapper.IMapper;

namespace remises_backend.Modulos
{
    public class ModuloAlive : NancyModule
    {
        public ModuloAlive(
            IRepositorio<PasajeroFepsa> repoPasajeroFepsa,
            IRepositorioPasajeros repoPasajeroViajes,
            IRepositorioPosiciones repoPosiciones,
            IRepositorio<Remisera> repoRemiseras,
            IRepositorio<VersionPasajero> repoVersionPasajeros,
            IRepositorioChoferes repoChoferes,
            IRepositorioViajes repoViajes,
            IRepositorio<ParametroLogica> repoLogica) : base()
        {
            Get[Rutas.Alive] = _ =>
            {
                var dto = new StatusAliveGet()
                {
                    version = Constantes.Version
                };
                return Response.AsJson(dto);
            };
            Get[Rutas.Fullcheck] = _ =>
            {
                var dto = new StatusFullGet()
                {
                    version = Constantes.Version,
                    cantidad_registros_choferes = repoChoferes.Count(),
                    cantidad_registros_parametros_logica = repoLogica.Count(),
                    cantidad_registros_pasajeros_fepsa = repoPasajeroFepsa.Count(),
                    cantidad_registros_pasajeros_viajes = repoPasajeroViajes.Count(),
                    cantidad_registros_posiciones = repoPosiciones.Count(),
                    cantidad_registros_remiseras = repoRemiseras.Count(),
                    cantidad_registros_versiones_pasajeros = repoVersionPasajeros.Count(),
                    cantidad_registros_viajes = repoViajes.Count()
                };
                    
                return Response.AsJson(dto);
            };
        }
    }
}
