﻿using System;
using AutoMapper;

namespace remises_backend.Infraestructura.Mappers
{
    using AccesoDatos.Tablas;
    using Infraestructura.Dtos;

    public static class MapperFactory
    {
        static MapperFactory()
        {
            Mapper.Initialize(cfg =>
            {                   
                cfg.CreateMap<ParametrosAppPost, ParametroLogica>()
                    .ForMember(dest => dest.Id, opt => opt.Ignore())
                    .ForMember(dest => dest.GpsInterval, m => m.MapFrom(src => src.gpsInterval))
                    .ForMember(dest => dest.DistanciaX1, m => m.MapFrom(src => src.distancia1))
                    .ForMember(dest => dest.DistanciaX2, m => m.MapFrom(src => src.distancia2))
                    .ForMember(dest => dest.DistanciaX3, m => m.MapFrom(src => src.distancia3))
                    .ForMember(dest => dest.OnOffDistanciaVelocidad, m => m.MapFrom(src => src.controlDistanciaVelocidad))
                    .ForMember(dest => dest.OnOffRumbo, m => m.MapFrom(src => src.controlRumbo))
                    .ForMember(dest => dest.OnOffTiempoVelocidad, m => m.MapFrom(src => src.controlTiempoVelocidad))
                    .ForMember(dest => dest.RumboDistancia, m => m.MapFrom(src => src.rumboDistancia))
                    .ForMember(dest => dest.RumboGrados, m => m.MapFrom(src => src.rumboGrados))
                    .ForMember(dest => dest.TiempoT1, m => m.MapFrom(src => src.tiempo1))
                    .ForMember(dest => dest.TiempoT2, m => m.MapFrom(src => src.tiempo2))
                    .ForMember(dest => dest.TiempoT3, m => m.MapFrom(src => src.tiempo3))
                    .ForMember(dest => dest.Velocidad1, m => m.MapFrom(src => src.velocidad1))
                    .ForMember(dest => dest.Velocidad2, m => m.MapFrom(src => src.velocidad2))
                    .ForMember(dest => dest.Velocidad3, m => m.MapFrom(src => src.velocidad3))
                    .ForMember(dest => dest.Velocidad4, m => m.MapFrom(src => src.velocidad4));

                cfg.CreateMap<ParametroLogica, ParametrosAppGet>()
                    .ForMember(dest => dest.gpsInterval, m => m.MapFrom(src => src.GpsInterval))
                    .ForMember(dest => dest.distancia1, m => m.MapFrom(src => src.DistanciaX1))
                    .ForMember(dest => dest.distancia2, m => m.MapFrom(src => src.DistanciaX2))
                    .ForMember(dest => dest.distancia3, m => m.MapFrom(src => src.DistanciaX3))
                    .ForMember(dest => dest.distancia3, m => m.MapFrom(src => src.DistanciaX3))
                    .ForMember(dest => dest.controlDistanciaVelocidad,
                        m => m.MapFrom(src => src.OnOffDistanciaVelocidad))
                    .ForMember(dest => dest.controlRumbo, m => m.MapFrom(src => src.OnOffRumbo))
                    .ForMember(dest => dest.controlTiempoVelocidad, m => m.MapFrom(src => src.OnOffTiempoVelocidad))
                    .ForMember(dest => dest.rumboDistancia, m => m.MapFrom(src => src.RumboDistancia))
                    .ForMember(dest => dest.rumboGrados, m => m.MapFrom(src => src.RumboGrados))
                    .ForMember(dest => dest.tiempo1, m => m.MapFrom(src => src.TiempoT1))
                    .ForMember(dest => dest.tiempo2, m => m.MapFrom(src => src.TiempoT2))
                    .ForMember(dest => dest.tiempo3, m => m.MapFrom(src => src.TiempoT3))
                    .ForMember(dest => dest.velocidad1, m => m.MapFrom(src => src.Velocidad1))
                    .ForMember(dest => dest.velocidad2, m => m.MapFrom(src => src.Velocidad2))
                    .ForMember(dest => dest.velocidad3, m => m.MapFrom(src => src.Velocidad3))
                    .ForMember(dest => dest.versionParametros, m => m.MapFrom(src => src.Id))
                    .ForMember(dest => dest.versionPasajeros, opt => opt.Ignore())
                    .ForMember(dest => dest.gpsInterval, m => m.MapFrom(src => src.GpsInterval));                
                
                cfg.CreateMap<PasajeroFepsa, PosiblePasajeroInner>()
                    .ForMember(dest => dest.nombre, m => m.MapFrom(src => src.NombreApellido))
                    .ForMember(dest => dest.pasajeroId, m => m.MapFrom(src => src.Id.ToString()))
                    .ForMember(dest => dest.legajo, m => m.MapFrom(src => src.Legajo.ToString()));

                cfg.CreateMap<Chofer, ActivacionChoferGet>()
                    .ForMember(dest => dest.token, opt => opt.UseValue("lala"))
                    .ForMember(dest => dest.chofer_id, m => m.MapFrom(src => src.Id.ToString()))
                    .ForMember(dest => dest.nombre, m => m.MapFrom(src => src.NombreApellido));

                cfg.CreateMap<ViajePost, Viaje>()
                    .ForMember(dest => dest.Id, opt => opt.UseValue(long.MinValue))
                    .ForMember(dest => dest.IdChofer, m => m.MapFrom(src => src.choferId))
                    .ForMember(dest => dest.FechaInicio, m => m.MapFrom(src => src.fechaInicio))
                    .ForMember(dest => dest.FechaFin, m => m.MapFrom(src => src.fechaFin))
                    .ForMember(dest => dest.TiempoEspera, m => m.MapFrom(src => src.tiempoEspera))
                    .ForMember(dest => dest.Peajes, m => m.MapFrom(src => src.peajes))
                    .ForMember(dest => dest.Ticket, m => m.MapFrom(src => src.ticket))
                    .ForMember(dest => dest.NumeroViaje, m => m.MapFrom(src => src.numeroViaje));

                cfg.CreateMap<PosicionPost, Posicion>()
                    .ForMember(dest => dest.IdViaje, opt => opt.Ignore())
                    .ForMember(dest => dest.IdViajeInterno, m => m.MapFrom(src => src.id))
                    .ForMember(dest => dest.Id, m => m.MapFrom(src => src.id))
                    .ForMember(dest => dest.fecha, m => m.MapFrom(src => src.fecha))
                    .ForMember(dest => dest.FechaInsercion, opt => opt.UseValue(DateTime.UtcNow))
                    .ForMember(dest => dest.IdVersionLogica, m => m.MapFrom(src => src.versionLogica))
                    .ForMember(dest => dest.latitud, m => m.MapFrom(src => src.latitud))
                    .ForMember(dest => dest.longitud, m => m.MapFrom(src => src.longitud))
                    .ForMember(dest => dest.Rumbo, m => m.MapFrom(src => src.rumbo))
                    .ForMember(dest => dest.Accuracy, m => m.MapFrom(src => src.accuracy))
                    .ForMember(dest => dest.Velocidad, m => m.MapFrom(src => src.velocidad));

                cfg.CreateMap<PasajeroPost, PasajeroViaje>()
                    .ForMember(dest => dest.Id, opt => opt.Ignore())
                    .ForMember(dest => dest.NombreApellido, m => m.MapFrom(src => src.pasajeroNombre))
                    .ForMember(dest => dest.IdPasajero, m => m.MapFrom(src => src.pasajeroId))
                    .ForMember(dest => dest.IdPosicionBaja, m => m.MapFrom(src => src.posBaja))
                    .ForMember(dest => dest.IdPosicionSube, m => m.MapFrom(src => src.posSube))
                    .ForMember(dest => dest.IdViaje, opt => opt.Ignore());
            });
        }

        public static IMapper GetInstance()
        {
            return Mapper.Instance;
        }
    }
}
