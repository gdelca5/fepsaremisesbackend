﻿using System;
using NPoco;
using remises_backend.Infraestructura;

namespace remises_backend.AccesoDatos.Tablas
{
    [TableName("pasajeros_viajes")]
    [PrimaryKey("id")]
    public class PasajeroViaje : Entidad
    {
        [Column("id")]
        public override long Id { get; set; }

        [Column("id_viaje")]
        public long IdViaje { get; set; }
        
        [Column("id_pasajero")]
        public long IdPasajero { get; set; }

        [Column("id_posicion_sube")]      
        public long IdPosicionSube { get; set; }

        [Column("id_posicion_baja")]
        public long IdPosicionBaja { get; set; }

        [Column("nombre_apellido")]
        public string NombreApellido { get; set; }

        public PasajeroViaje() { }
    }
}
