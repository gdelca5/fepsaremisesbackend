﻿using System;
using NPoco;
using remises_backend.Infraestructura;

namespace remises_backend.AccesoDatos.Tablas
{
    [TableName("remiseras")]
    [PrimaryKey("id", AutoIncrement = true)]
    public class Remisera : Entidad
    {
        [Column("id")]
        public override long Id { get; set; }

        [Column("codigo_activacion")]
        public string CodigoActivacion { get; set; }

        [Column("vigente")]
        public bool Vigente { get; set; }   

        public Remisera() { }
    }
}
