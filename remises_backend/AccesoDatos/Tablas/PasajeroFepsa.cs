﻿using System;
using NPoco;
using remises_backend.Infraestructura;

namespace remises_backend.AccesoDatos.Tablas
{
    [TableName("pasajeros_fepsa")]
    [PrimaryKey("id", AutoIncrement = true)]
    public class PasajeroFepsa : Entidad
    {
        [Column("id")]
        public override long Id { get; set; }
        
        [Column("legajo")]
        public string Legajo { get; set; }
        
        [Column("nombre_apellido")]
        public string NombreApellido { get; set; }
        
        [Column("id_version")]
        public long IdVersion { get; set; }        

        public PasajeroFepsa() { }
    }
}
