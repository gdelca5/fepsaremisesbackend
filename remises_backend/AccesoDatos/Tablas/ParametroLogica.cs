﻿using System;
using NPoco;
using NPoco.FluentMappings;

namespace remises_backend.AccesoDatos.Tablas
{
    using remises_backend.Infraestructura;

    // Parametros y versiones de la app "seguimiento de remises"
    [TableName("parametros_logica")]
    [PrimaryKey("id", AutoIncrement = true)]
    public class ParametroLogica : Entidad
    {
        // por que no duele tener un id
        [Column("id")]
        public override long Id { get; set; }
                
        // Filtro rumbo
        [Column("on_off_rumbo")]
        public bool OnOffRumbo { get; set; }

        // Filtro Tiempo y velocidad
        [Column("on_off_tiempo_velocidad")]
        public bool OnOffTiempoVelocidad { get; set; }

        // Filtro distancia y velocidad
        [Column("on_off_distancia_velocidad")]
        public bool OnOffDistanciaVelocidad { get; set; }

        // parametro velocidad en mts/seg
        [Column("velocidad_v1")]
        public decimal Velocidad1 { get; set; }

        // parametro velocidad en mts/seg
        [Column("velocidad_v2")]
        public decimal Velocidad2 { get; set; }

        // parametro velocidad en mts/seg
        [Column("velocidad_v3")]
        public decimal Velocidad3 { get; set; }

        //parametro velocidad en mts/seg
        [Column("velocidad_v4")]
        public decimal Velocidad4 { get; set; }

        [Column("tiempo_t1")]
        //tiempo en segundos
        public decimal TiempoT1 { get; set; }

        //tiempo en segundos
        [Column("tiempo_t2")]
        public decimal TiempoT2 { get; set; }

        //tiempo en segundos
        [Column("tiempo_t3")]
        public decimal TiempoT3 { get; set; }

        //distancia en metros
        [Column("distancia_x1")]
        public decimal DistanciaX1 { get; set; }

        //distancia en metros
        [Column("distancia_x2")]
        public decimal DistanciaX2 { get; set; }

        //distancia en metros
        [Column("distancia_x3")]
        public decimal DistanciaX3 { get; set; }

        //Posicion radial en grados
        [Column("rumbo_grados_rg")]
        public decimal RumboGrados { get; set; }

        //Distancia minima para almacenar un punto al tener cambio de rumbo.En metros
        [Column("rumbo_distancia_rd")]
        public decimal RumboDistancia { get; set; }

        //Intervalo de refresco del servicio de GPS en segundos.
        [Column("gps_interval")]
        public int GpsInterval { get; set; }
        
        public ParametroLogica() { }
    }
}
