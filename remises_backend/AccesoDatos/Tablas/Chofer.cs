﻿using System;
using NPoco;
using remises_backend.Infraestructura;

namespace remises_backend.AccesoDatos.Tablas
{
    [TableName("choferes")]
    [PrimaryKey("id", AutoIncrement = true)]
    public class Chofer : Entidad
    {
        [Column("id")]
        public override long Id { get; set; }

        [Column("id_remisera")]
        public long IdRemisera { get;  set;}

        [Column("nombre_apellido")]
        public string NombreApellido { get; set; }

        [Column("dni")]
        public string DNI { get; set; }

        [Column("vigente")]
        public bool Vigente { get; set; }   

        public Chofer() { }
    }
}
