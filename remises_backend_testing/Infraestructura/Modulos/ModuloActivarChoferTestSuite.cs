﻿using System;
using System.Linq;
using Nancy.Bootstrapper;
using Nancy;
using Nancy.Testing;
using Xunit;
using FakeItEasy;
using AutoMapper;
using NPoco;
using System.Collections.Generic;
using remises_backend.Infraestructura.Mappers;
using remises_backend.Modulos;

namespace remises_backend_testing.Infraestructura.Modulos
{
    using remises_backend.Infraestructura.Modulos;
    using remises_backend.AccesoDatos.Tablas;
    using remises_backend.Infraestructura.Repositorios;
    using remises_backend.Infraestructura.Dtos;
    using Newtonsoft.Json;

    public class ModuloActivarChoferTestSuite
    {
        [Fact]
        public void deberia_poder_ser_instanciado()
        {
            // given
            var fakeRepo = A.Fake<IRepositorioChoferes>();
            var fakeRepoRemiseras = A.Fake<IRepositorioRemiseras>();
            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<IRepositorioRemiseras>(fakeRepoRemiseras);
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorioChoferes>(fakeRepo);
                with.Module<ModuloActivarChofer>();
            });

            // when
            new Browser(bootstrapper);
        }

        [Fact]
        public void deberia_fallar_sin_body()
        {
            // given
            var fakeRepo = A.Fake<IRepositorioChoferes>();
            var fakeRepoRemiseras = A.Fake<IRepositorioRemiseras>();
            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<IRepositorioRemiseras>(fakeRepoRemiseras);
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorioChoferes>(fakeRepo);
                with.Module<ModuloActivarChofer>();
            });

            var browser = new Browser(bootstrapper);

            // when
            Action result = () =>
            {
                browser.Post(Rutas.ActivarChofer, with =>
                {
                    with.HttpRequest();
                });
            };

            //then
            Assert.Throws<Exception>(result);
        }

        [Fact]
        public void deberia_fallar_al_llamar_sin_parametro_nombre()
        {
            // given
            var fakeRepo = A.Fake<IRepositorioChoferes>();
            var fakeRepoRemiseras = A.Fake<IRepositorioRemiseras>();
            var soloClaveJson = @"{ 'clave' : '123'}";
            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<IRepositorioRemiseras>(fakeRepoRemiseras);
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorioChoferes>(fakeRepo);
                with.Module<ModuloActivarChofer>();
            });
              
            var browser = new Browser(bootstrapper);

            // when
            Action result = () =>
            {
                browser.Post(Rutas.ActivarChofer, with =>
                {
                    with.HttpRequest();
                    with.Body(soloClaveJson);
                });
            };
            
            //then
            Assert.Throws<Exception>(result);
        }
        
        [Fact]
        public void deberia_fallar_al_llamar_sin_parametro_clave()
        {
            // given
            var fakeRepo = A.Fake<IRepositorioChoferes>();
            var fakeRepoRemiseras = A.Fake<IRepositorioRemiseras>();
            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var bodyConSoloNombre = @" { 'nombre' : 'etc' } ";
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<IRepositorioRemiseras>(fakeRepoRemiseras);
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorioChoferes>(fakeRepo);
                with.Module<ModuloActivarChofer>();
            });
              
            var browser = new Browser(bootstrapper);

            // when
            Action result = () =>
            {
                browser.Post(Rutas.ActivarChofer, with =>
                {
                    with.HttpRequest();
                    with.Body(bodyConSoloNombre);
                });
            };
            
            //then
            Assert.Throws<Exception>(result);
        }

        [Fact]
        public void no_deberia_fallar_si_el_chofer_no_existe()
        {
            // given
            var fakeRepo = A.Fake<IRepositorioChoferes>();
            var fakeRepoRemiseras = A.Fake<IRepositorioRemiseras>();

            var fakeRemisera = A.Fake<Remisera>();
            var dniChofer = "123123123";
            var clavePrecompartida = "2123";
            var algunNombreDeChofer = "pepe";

            var body = JsonConvert.SerializeObject(
                new ActivarChoferPost()
                {
                    nombre = algunNombreDeChofer,
                    dni = dniChofer,
                    clave = clavePrecompartida
                });

            A.CallTo(() => fakeRepo.buscarChofer(A<String>._, A<String>._))
             .Returns(null);
            A.CallTo(() => fakeRepoRemiseras.buscarRemisera(clavePrecompartida))
             .Returns(fakeRemisera);

            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<IRepositorioRemiseras>(fakeRepoRemiseras);
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorioChoferes>(fakeRepo);
                with.Module<ModuloActivarChofer>();
            });

            var browser = new Browser(bootstrapper);

            // when
            var result = browser.Post(Rutas.ActivarChofer, with =>
            {
                with.HttpRequest();
                with.Body(body);
            });

            //then
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        }

        [Fact]
        public void deberia_llamar_al_repo_correctamente()
        {
            // given
            var fakeRepo = A.Fake<IRepositorioChoferes>();
            var fakeRepoRemiseras = A.Fake<IRepositorioRemiseras>();

            var fakeChofer = A.Fake<Chofer>();
            var dniChofer = "123123123";
            var clavePrecompartida = "2123";
            var body = JsonConvert.SerializeObject(
                new ActivarChoferPost()
                {
                    dni = dniChofer,
                    clave = clavePrecompartida
                });

            A.CallTo(() => fakeRepo.buscarChofer(A<String>._, A<String>._)).Returns(fakeChofer);

            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<IRepositorioRemiseras>(fakeRepoRemiseras);
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorioChoferes>(fakeRepo);
                with.Module<ModuloActivarChofer>();
            });

            var browser = new Browser(bootstrapper);

            // when
            var result = browser.Post(Rutas.ActivarChofer, with =>
                {
                    with.HttpRequest();
                    with.Body(body);
                });

            //then
            A.CallTo(() => fakeRepo.buscarChofer(A<String>._, A<String>._)).MustHaveHappened();
        }

        [Fact]
        public void deberia_fallar_si_la_remisera_no_existe()
        {
            // given
            var fakeRepo = A.Fake<IRepositorioChoferes>();
            var fakeRepoRemiseras = A.Fake<IRepositorioRemiseras>();

            var clavePrecompartidaDeRemiseraQueNoExiste = "2123";
            var algunDNI = "123123123";

            var body = JsonConvert.SerializeObject(
                new ActivarChoferPost()
                {
                  dni = algunDNI,
                  clave = clavePrecompartidaDeRemiseraQueNoExiste
                });

            A.CallTo(() => fakeRepo.buscarChofer(algunDNI, clavePrecompartidaDeRemiseraQueNoExiste))
             .Returns(null);            
            A.CallTo(() => fakeRepoRemiseras.buscarRemisera(clavePrecompartidaDeRemiseraQueNoExiste))
             .Returns(null);

            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<IRepositorioRemiseras>(fakeRepoRemiseras);
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorioChoferes>(fakeRepo);
                with.Module<ModuloActivarChofer>();
            });

            var browser = new Browser(bootstrapper);

            // when
            Action result = () =>
            {
                browser.Post(Rutas.ActivarChofer, with =>
                {
                    with.HttpRequest();
                    with.Body(body);
                });
            };

            //then
            Assert.Throws<Exception>(result);
        }

        [Fact]
        public void no_deberia_fallar_si_el_chofer_existe()
        {
            // given
            var fakeRepo = A.Fake<IRepositorioChoferes>();
            var fakeRepoRemiseras = A.Fake<IRepositorioRemiseras>();
            var fakeChofer = A.Fake<Chofer>();
            var dniChofer = "123123123";
            var clavePrecompartida = "2123";
            var body = JsonConvert.SerializeObject(
                new ActivarChoferPost()
                {
                    dni = dniChofer,
                    clave = clavePrecompartida
                });

            A.CallTo(() => fakeRepo.buscarChofer(A<String>._, A<String>._)).Returns(fakeChofer);

            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<IRepositorioRemiseras>(fakeRepoRemiseras);
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorioChoferes>(fakeRepo);
                with.Module<ModuloActivarChofer>();
            });

            var browser = new Browser(bootstrapper);

            // when
            var result = browser.Post(Rutas.ActivarChofer, with =>
            {
                with.HttpRequest();
                with.Body(body);
            });

            //then
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        }

        [Fact]
        public void no_deberia_insertar_nuevo_chofer_si_existe_previamente()
        {
            // given
            var fakeRepo = A.Fake<IRepositorioChoferes>();
            var fakeRepoRemiseras = A.Fake<IRepositorioRemiseras>();
            var fakeChofer = A.Fake<Chofer>();
            var dniChofer = "123123123";
            var clavePrecompartida = "2123";
            var body = JsonConvert.SerializeObject(
                new ActivarChoferPost()
                {
                    dni = dniChofer,
                    clave = clavePrecompartida
                });

            A.CallTo(() => fakeRepo.buscarChofer(A<String>._, A<String>._)).Returns(fakeChofer);

            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<IRepositorioRemiseras>(fakeRepoRemiseras);
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorioChoferes>(fakeRepo);
                with.Module<ModuloActivarChofer>();
            });

            var browser = new Browser(bootstrapper);

            // when
            var result = browser.Post(Rutas.ActivarChofer, with =>
            {
                with.HttpRequest();
                with.Body(body);
            });

            //then
            A.CallTo(() => fakeRepoRemiseras.buscarRemisera(A<String>._)).MustNotHaveHappened();
            A.CallTo(() => fakeRepo.guardarChofer(A<Chofer>._)).MustNotHaveHappened();
        }

        [Fact]
        public void deberia_fallar_al_insertar_nuevo_chofer_si_no_tiene_nombre()
        {
            // given
            var fakeRepo = A.Fake<IRepositorioChoferes>();
            var fakeRepoRemiseras = A.Fake<IRepositorioRemiseras>();
            var fakeChofer = A.Fake<Chofer>();
            var fakeRemisera = A.Fake<Remisera>();

            var dniChofer = "123123123";
            var clavePrecompartida = "2123";
            String nombreVacio = null;

            var body = JsonConvert.SerializeObject(
                new ActivarChoferPost()
                {
                    nombre = nombreVacio,
                    dni = dniChofer,
                    clave = clavePrecompartida
                });

            A.CallTo(() => fakeRepo.buscarChofer(A<String>._, A<String>._)).Returns(null);
            A.CallTo(() => fakeRepoRemiseras.buscarRemisera(clavePrecompartida)).Returns(fakeRemisera);

            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<IRepositorioRemiseras>(fakeRepoRemiseras);
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorioChoferes>(fakeRepo);
                with.Module<ModuloActivarChofer>();
            });

            var browser = new Browser(bootstrapper);

            // when
            Action result = () =>
            {
                browser.Post(Rutas.ActivarChofer, with =>
                {
                    with.HttpRequest();
                    with.Body(body);
                });
            };

            //then
            Assert.Throws<Exception>(result);
        }

        [Fact]
        public void deberia_fallar_al_insertar_nuevo_chofer_si_dni_es_vacio()
        {
            // given
            var fakeRepo = A.Fake<IRepositorioChoferes>();
            var fakeRepoRemiseras = A.Fake<IRepositorioRemiseras>();
            var fakeChofer = A.Fake<Chofer>();
            var fakeRemisera = A.Fake<Remisera>();

            String dniVacio = null;
            var clavePrecompartida = "2123";
            var algunNombre = "pepe";

            var body = JsonConvert.SerializeObject(
                new ActivarChoferPost()
                {
                    nombre = algunNombre,
                    dni = dniVacio,
                    clave = clavePrecompartida
                });

            A.CallTo(() => fakeRepo.buscarChofer(A<String>._, A<String>._)).Returns(null);
            A.CallTo(() => fakeRepoRemiseras.buscarRemisera(clavePrecompartida)).Returns(fakeRemisera);

            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<IRepositorioRemiseras>(fakeRepoRemiseras);
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorioChoferes>(fakeRepo);
                with.Module<ModuloActivarChofer>();
            });

            var browser = new Browser(bootstrapper);

            // when
            Action result = () =>
            {
                browser.Post(Rutas.ActivarChofer, with =>
                {
                    with.HttpRequest();
                    with.Body(body);
                });
            };

            //then
            Assert.Throws<Exception>(result);
        }

        [Fact]
        public void deberia_guardar_el_chofer_si_no_existe_previamente()
        {
            // given
            var fakeRepo = A.Fake<IRepositorioChoferes>();
            var fakeRepoRemiseras = A.Fake<IRepositorioRemiseras>();

            var fakeChofer = A.Fake<Chofer>();
            var fakeRemisera = A.Fake<Remisera>();

            var dniChofer = "123123123";
            var clavePrecompartida = "2123";
            var nombreChofer = "Pepe";
            long fakeIdRemisera = 2;

            var body = JsonConvert.SerializeObject(
                new ActivarChoferPost()
                {
                    nombre = nombreChofer,
                    dni = dniChofer,
                    clave = clavePrecompartida
                });

            A.CallTo(() => fakeRepo.buscarChofer(A<String>._, A<String>._)).Returns(null);
            A.CallTo(() => fakeRepoRemiseras.buscarRemisera(A<String>._)).Returns(fakeRemisera);
            A.CallTo(() => fakeRemisera.Id).Returns(fakeIdRemisera);

            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<IRepositorioRemiseras>(fakeRepoRemiseras);
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorioChoferes>(fakeRepo);
                with.Module<ModuloActivarChofer>();
            });

            var browser = new Browser(bootstrapper);

            // when
            var result = browser.Post(Rutas.ActivarChofer, with =>
            {
                with.HttpRequest();
                with.Body(body);
            });

            //then
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);

            A.CallTo(() => fakeRepoRemiseras.buscarRemisera(clavePrecompartida)).MustHaveHappened();

            Predicate<Chofer> verificarChofer = delegate (Chofer chofer)
            {
                Assert.NotNull(chofer);
                Assert.Equal(dniChofer, chofer.DNI);
                Assert.Equal(nombreChofer, chofer.NombreApellido);
                Assert.Equal(fakeIdRemisera, chofer.IdRemisera);
                Assert.Equal(true, chofer.Vigente);
                return true;
            };

            //then
            A.CallTo(
                () => fakeRepo.guardarChofer(A<Chofer>.That.Matches(v => verificarChofer(v))))
                .MustHaveHappened();
        }

        [Fact]
        public void deberia_llamar_al_mapper()
        {
            // given
            var fakeRepo = A.Fake<IRepositorioChoferes>();
            var fakeRepoRemiseras = A.Fake<IRepositorioRemiseras>();
            var fakeChofer = A.Fake<Chofer>();
            var dniChofer = "123123123";
            var clavePrecompartida = "2123";
            var body = JsonConvert.SerializeObject(
                new ActivarChoferPost()
                {
                    dni = dniChofer,
                    clave = clavePrecompartida
                });

            A.CallTo(() => fakeRepo.buscarChofer(A<String>._, A<String>._)).Returns(fakeChofer);

            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<IRepositorioRemiseras>(fakeRepoRemiseras);
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorioChoferes>(fakeRepo);
                with.Module<ModuloActivarChofer>();
            });

            var browser = new Browser(bootstrapper);

            // when
            var result = browser.Post(Rutas.ActivarChofer, with =>
            {
                with.HttpRequest();
                with.Body(body);
            });

            //then
            A.CallTo(() => fakeMapper.Map<Chofer, ActivacionChoferGet>(A<Chofer>._)).MustHaveHappened();
        }

        [Fact]
        public void el_dto_deberia_ser_el_esperado()
        {
            // given
            long fakeIdRemisera = 1;
            var remiseraFake = A.Fake<Remisera>();
            A.CallTo(() => remiseraFake.Id).Returns(fakeIdRemisera);

            var fakeRepoRemiseras = A.Fake<IRepositorioRemiseras>();

            var dniChofer = "20321654";
            var chofer = new Chofer() {
                Id = 1,
                IdRemisera = fakeIdRemisera,
                NombreApellido = "pepe",
                DNI = dniChofer,
                Vigente = true
            };
            var clavePrecompartida = "2123";
            
            var body = JsonConvert.SerializeObject(
                new ActivarChoferPost()
                {
                    dni = dniChofer,
                    clave = clavePrecompartida
                });

            var fakeRepo = A.Fake<IRepositorioChoferes>();
            A.CallTo(() => fakeRepo.buscarChofer(A<String>._, A<String>._)).Returns(chofer);        

            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<IRepositorioRemiseras>(fakeRepoRemiseras);
                with.Dependency<AutoMapper.IMapper>(MapperFactory.GetInstance());
                with.Dependency<IRepositorioChoferes>(fakeRepo);
                with.Module<ModuloActivarChofer>();
            });

            var browser = new Browser(bootstrapper);

            // when
            var result = browser.Post(Rutas.ActivarChofer, with =>
            {
                with.HttpRequest();
                with.Body(body);
            });

            //then
            var actual = result.Body.DeserializeJson<ActivacionChoferGet>();

            Assert.Equal(chofer.Id.ToString(), actual.chofer_id);
            Assert.Equal(chofer.NombreApellido.ToString(), actual.nombre);
        }
    }
}
