﻿using System;
using System.Collections;
using System.Linq;
using Nancy.Bootstrapper;
using Nancy;
using Nancy.Testing;
using Xunit;
using FakeItEasy;
using AutoMapper;
using NPoco;
using System.Collections.Generic;
using CsQuery;
using remises_backend.Infraestructura.Mappers;
using remises_backend.Modulos;
using System.Data.SqlClient;

namespace remises_backend_testing.Infraestructura.Modulos
{
    using remises_backend.Infraestructura.Modulos;
    using remises_backend.AccesoDatos.Tablas;
    using remises_backend.Infraestructura.Repositorios;
    using remises_backend.Infraestructura.Dtos;
    using Newtonsoft.Json;

    public class ModuloAgregarViajeTestSuite
    {
        private readonly static String EmptyJson = "{}";

        [Fact]
        public void deberia_poder_ser_instanciado()
        {
            // given
            var fakeRepo = A.Fake<IRepositorioViajes>();
            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorioViajes>(fakeRepo);
                with.Dependency<IRepositorioPasajeros>(A.Fake<IRepositorioPasajeros>());
                with.Dependency<IRepositorioPosiciones>(A.Fake<IRepositorioPosiciones>());
                with.Module<ModuloViajes>();
            });

            // when
            new Browser(bootstrapper);
        }

        [Fact]
        public void deberia_fallar_sin_json_en_el_body()
        {
            // given
            var jsonInvalido = "";
            var fakeRepo = A.Fake<IRepositorioViajes>();
            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorioViajes>(fakeRepo);
                with.Dependency<IRepositorioPasajeros>(A.Fake<IRepositorioPasajeros>());
                with.Dependency<IRepositorioPosiciones>(A.Fake<IRepositorioPosiciones>());
                with.Module<ModuloViajes>();
            });
            var browser = new Browser(bootstrapper);

            // when
            Action result = () =>
            {
                browser.Post(Rutas.Viajes, with =>
                {
                    with.HttpRequest();
                    with.Body(jsonInvalido);
                });
            };

            //then
            Assert.Throws<Exception>(result);
        }

        [Fact]
        public void no_deberia_fallar_ante_pedido_valido()
        {
            // given            
            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var fakeRepo = A.Fake<IRepositorioViajes>();
            var dto = new ViajePost()
            {
                fechaInicio = DateTime.UtcNow,
                fechaFin = DateTime.UtcNow.AddHours(1),
                choferId = 1,
                tiempoEspera = 2,
                peajes = 30
            };

            var jsonValido = JsonConvert.SerializeObject(dto, Formatting.Indented);

            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(MapperFactory.GetInstance());
                with.Dependency<IRepositorioViajes>(fakeRepo);
                with.Dependency<IRepositorioPasajeros>(A.Fake<IRepositorioPasajeros>());
                with.Dependency<IRepositorioPosiciones>(A.Fake<IRepositorioPosiciones>());
                with.Module<ModuloViajes>();
            });
            var browser = new Browser(bootstrapper);

            // when
            var result = browser.Post(Rutas.Viajes, with =>
            {
                with.HttpRequest();
                with.Body(jsonValido);
            });

            //then
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        }

        [Fact]
        public void no_deberia_fallar_ante_viaje_repetido()
        {
            // given            
            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var fakeRepo = A.Fake<IRepositorioViajes>();
            var dto = new ViajePost()
            {
                fechaInicio = DateTime.UtcNow,
                fechaFin = DateTime.UtcNow.AddHours(1),
                choferId = 1,
                tiempoEspera = 2,
                peajes = 30
            };

            var jsonValido = JsonConvert.SerializeObject(dto, Formatting.Indented);

            A.CallTo(() => fakeRepo.guardarViaje(A<Viaje>._)).Throws(MakeSqlException);

            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(MapperFactory.GetInstance());
                with.Dependency<IRepositorioViajes>(fakeRepo);
                with.Dependency<IRepositorioPasajeros>(A.Fake<IRepositorioPasajeros>());
                with.Dependency<IRepositorioPosiciones>(A.Fake<IRepositorioPosiciones>());
                with.Module<ModuloViajes>();
            });
            var browser = new Browser(bootstrapper);

            // when
            var result = browser.Post(Rutas.Viajes, with =>
            {
                with.HttpRequest();
                with.Body(jsonValido);
            });

            //then
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        }

        [Fact]
        public void deberia_devolver_json_vacio_ante_pedido_valido()
        {
            // given            
            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var fakeRepo = A.Fake<IRepositorioViajes>();
            var dto = new ViajePost()
            {
                fechaInicio = DateTime.UtcNow,
                fechaFin = DateTime.UtcNow.AddHours(1),
                choferId = 1,
                tiempoEspera = 2,
                peajes = 30
            };

            var jsonValido = JsonConvert.SerializeObject(dto, Formatting.Indented);

            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(MapperFactory.GetInstance());
                with.Dependency<IRepositorioViajes>(fakeRepo);
                with.Dependency<IRepositorioPasajeros>(A.Fake<IRepositorioPasajeros>());
                with.Dependency<IRepositorioPosiciones>(A.Fake<IRepositorioPosiciones>());
                with.Module<ModuloViajes>();
            });
            var browser = new Browser(bootstrapper);

            // when
            var result = browser.Post(Rutas.Viajes, with =>
            {
                with.HttpRequest();
                with.Body(jsonValido);
            });

            //then
            Assert.Equal(EmptyJson, result.Body.AsString());
        }

        [Fact]
        public void deberia_guardar_el_viaje()
        {
            // given
            var dto = new ViajePost()
            {
                fechaInicio = DateTime.UtcNow,
                fechaFin = DateTime.UtcNow.AddHours(1),
                choferId = 1,
                tiempoEspera = 2,
                peajes = 30,
                ticket = 1000
            };
            var jsonValido = JsonConvert.SerializeObject(dto, Formatting.Indented);

            var fakeRepo = A.Fake<IRepositorioViajes>();

            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(MapperFactory.GetInstance());
                with.Dependency<IRepositorioViajes>(fakeRepo);
                with.Dependency<IRepositorioPasajeros>(A.Fake<IRepositorioPasajeros>());
                with.Dependency<IRepositorioPosiciones>(A.Fake<IRepositorioPosiciones>());
                with.Module<ModuloViajes>();
            });
            var browser = new Browser(bootstrapper);

            // when
            var result = browser.Post(Rutas.Viajes, with =>
            {
                with.HttpRequest();
                with.Body(jsonValido);
            });

            Predicate<Viaje> verificarViaje = delegate(Viaje viaje)
            {
                Assert.Equal(dto.choferId, viaje.IdChofer);
                Assert.Equal(dto.fechaInicio, viaje.FechaInicio);
                Assert.Equal(dto.fechaFin, viaje.FechaFin);
                Assert.Equal(dto.tiempoEspera, viaje.TiempoEspera);
                Assert.Equal(dto.peajes, viaje.Peajes);
                Assert.Equal(dto.ticket, viaje.Ticket);
                return true;
            };

            //then
            A.CallTo(
                () => fakeRepo.guardarViaje(A<Viaje>.That.Matches(v => verificarViaje(v)))).MustHaveHappened();
        }
        
        [Fact]
        public void deberia_guardar_las_posiciones()
        {
            // given
            var posicion = new PosicionPost()
            {
                latitud = 1,
                longitud = 2,
                velocidad = 3,
                rumbo = 4,
                fecha = DateTime.UtcNow,
                versionLogica = 5
            };

            var dto = new ViajePost()
            {
                fechaInicio = DateTime.UtcNow,
                fechaFin = DateTime.UtcNow.AddHours(1),
                choferId = 1,
                tiempoEspera = 2,
                peajes = 30,
                posiciones = new PosicionPost[] {posicion}.ToList()
            };

            var jsonValido = JsonConvert.SerializeObject(dto, Formatting.Indented);

            var fakeRepo = A.Fake<IRepositorioPosiciones>();

            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(MapperFactory.GetInstance());
                with.Dependency<IRepositorioPosiciones>(fakeRepo);
                with.Dependency<IRepositorioViajes>(A.Fake<IRepositorioViajes>());
                with.Dependency<IRepositorioPasajeros>(A.Fake<IRepositorioPasajeros>());
                with.Module<ModuloViajes>();
            });
            var browser = new Browser(bootstrapper);

            // when
            var result = browser.Post(Rutas.Viajes, with =>
            {
                with.HttpRequest();
                with.Body(jsonValido);
            });

            Predicate<List<Posicion>> verificarPosiciones = delegate(List<Posicion> posiciones)
            {
                Assert.Equal(1, posiciones.Count);
                Posicion p = posiciones[0];
                Assert.Equal(posicion.latitud, p.latitud);
                Assert.Equal(posicion.longitud, p.longitud);
                Assert.Equal(posicion.versionLogica, p.IdVersionLogica);
                Assert.Equal(posicion.rumbo, p.Rumbo);
                return true;
            };

            //then
            A.CallTo(
                    () => fakeRepo.guardarPosiciones(A<List<Posicion>>.That.Matches(v => verificarPosiciones(v))))
                .MustHaveHappened();
        }

        [Fact]
        public void deberia_devolver_json_vacio_con_viaje_valido_con_posiciones()
        {
            // given
            var posicion = new PosicionPost()
            {
                latitud = 1,
                longitud = 2,
                velocidad = 3,
                rumbo = 4,
                fecha = DateTime.UtcNow,
                versionLogica = 5
            };

            var dto = new ViajePost()
            {
                fechaInicio = DateTime.UtcNow,
                fechaFin = DateTime.UtcNow.AddHours(1),
                choferId = 1,
                tiempoEspera = 2,
                peajes = 30,
                posiciones = new PosicionPost[] { posicion }.ToList()
            };

            var jsonValido = JsonConvert.SerializeObject(dto, Formatting.Indented);

            var fakeRepo = A.Fake<IRepositorioPosiciones>();

            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(MapperFactory.GetInstance());
                with.Dependency<IRepositorioPosiciones>(fakeRepo);
                with.Dependency<IRepositorioViajes>(A.Fake<IRepositorioViajes>());
                with.Dependency<IRepositorioPasajeros>(A.Fake<IRepositorioPasajeros>());
                with.Module<ModuloViajes>();
            });
            var browser = new Browser(bootstrapper);

            // when
            var result = browser.Post(Rutas.Viajes, with =>
            {
                with.HttpRequest();
                with.Body(jsonValido);
            });

            Predicate<List<Posicion>> verificarPosiciones = delegate (List<Posicion> posiciones)
            {
                Assert.Equal(1, posiciones.Count);
                Posicion p = posiciones[0];
                Assert.Equal(posicion.latitud, p.latitud);
                Assert.Equal(posicion.longitud, p.longitud);
                Assert.Equal(posicion.versionLogica, p.IdVersionLogica);
                Assert.Equal(posicion.rumbo, p.Rumbo);
                return true;
            };

            //then
            Assert.Equal(EmptyJson, result.Body.AsString());
        }

        [Fact]
        public void deberia_guardar_a_los_pasajeros()
        {
            // given
            var posicion = new PosicionPost()
            {
                id = 1,
                latitud = 1,
                longitud = 2,
                velocidad = 3,
                rumbo = 4,
                fecha = DateTime.UtcNow,
                versionLogica = 5
            };

            var pasajeroConocido = new PasajeroPost()
            {
                pasajeroId = 1,
                posBaja = 1,
                posSube = 1
            };
            
            var pasajeroDesconocido = new PasajeroPost()
            {
                pasajeroNombre = "pepe",
                posBaja = 1,
                posSube = 1
            };

            var dto = new ViajePost()
            {
                fechaInicio = DateTime.UtcNow,
                fechaFin = DateTime.UtcNow.AddHours(1),
                choferId = 1,
                tiempoEspera = 2,
                peajes = 30,
                posiciones = new PosicionPost[] {posicion}.ToList(),
                pasajeros = new PasajeroPost[] {pasajeroConocido, pasajeroDesconocido}.ToList()
            };

            var jsonValido = JsonConvert.SerializeObject(dto, Formatting.Indented);

            var fakeRepo = A.Fake<IRepositorioPasajeros>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(MapperFactory.GetInstance());
                with.Dependency<IRepositorioPasajeros>(fakeRepo);
                with.Dependency<IRepositorioPosiciones>(A.Fake<IRepositorioPosiciones>());
                with.Dependency<IRepositorioViajes>(A.Fake<IRepositorioViajes>());
                with.Module<ModuloViajes>();
            });
            var browser = new Browser(bootstrapper);

            // when
            var result = browser.Post(Rutas.Viajes, with =>
            {
                with.HttpRequest();
                with.Body(jsonValido);
            });

            Predicate<List<PasajeroViaje>> verificarPasajeros = delegate(List<PasajeroViaje> pasajeros)
            {
                Assert.Equal(2, pasajeros.Count);
                PasajeroViaje p1 = pasajeros[0];
                Assert.Equal(pasajeroConocido.pasajeroId.Value, p1.IdPasajero);
                PasajeroViaje p2 = pasajeros[1];
                Assert.Equal(pasajeroDesconocido.pasajeroNombre, p2.NombreApellido);

                return true;
            };

            //then
            A.CallTo(
                    () => fakeRepo.guardarPasajeros(A<List<PasajeroViaje>>.That.Matches(v => verificarPasajeros(v))))
                .MustHaveHappened();
        }

        [Fact]
        public void deberia_guardar_usando_el_id_viaje_correcto_en_posiciones()
        {
            // given
            var posicion = new PosicionPost()
            {
                latitud = 1,
                longitud = 2,
                velocidad = 3,
                rumbo = 4,
                accuracy = 10,
                fecha = DateTime.UtcNow,
                versionLogica = 5
            };

            var dto = new ViajePost()
            {
                fechaInicio = DateTime.UtcNow,
                fechaFin = DateTime.UtcNow.AddHours(1),
                choferId = 1,
                tiempoEspera = 2,
                peajes = 30,
                posiciones = new PosicionPost[] {posicion}.ToList()
            };

            var jsonValido = JsonConvert.SerializeObject(dto, Formatting.Indented);

            var fakeRepo = A.Fake<IRepositorioPosiciones>();

            var idViajeEsperado = 1008L;

            var fakeRepoViaje = A.Fake<IRepositorioViajes>();
            A.CallTo(() => fakeRepoViaje.guardarViaje(A<Viaje>._)).Returns(idViajeEsperado);

            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(MapperFactory.GetInstance());
                with.Dependency<IRepositorioPosiciones>(fakeRepo);
                with.Dependency<IRepositorioViajes>(fakeRepoViaje);
                with.Dependency<IRepositorioPasajeros>(A.Fake<IRepositorioPasajeros>());
                with.Module<ModuloViajes>();
            });
            var browser = new Browser(bootstrapper);

            // when
            var result = browser.Post(Rutas.Viajes, with =>
            {
                with.HttpRequest();
                with.Body(jsonValido);
            });

            Predicate<List<Posicion>> verificarPosiciones = delegate(List<Posicion> posiciones)
            {
                Assert.Equal(1, posiciones.Count);
                Posicion p = posiciones[0];
                Assert.Equal(idViajeEsperado, p.IdViaje);
                return true;
            };

            //then
            A.CallTo(
                    () => fakeRepo.guardarPosiciones(A<List<Posicion>>.That.Matches(v => verificarPosiciones(v))))
                .MustHaveHappened();
        }

        [Fact]
        public void deberia_devolver_json_vacio_al_guardar_viaje_con_pasajeros()
        {
            // given
            var posicion = new PosicionPost()
            {
                id = 1,
                latitud = 1,
                longitud = 2,
                velocidad = 3,
                rumbo = 4,
                accuracy = 2,
                fecha = DateTime.UtcNow,
                versionLogica = 5
            };

            var pasajeroConocido = new PasajeroPost()
            {
                pasajeroId = 1,
                posBaja = 1,
                posSube = 1
            };

            var pasajeroDesconocido = new PasajeroPost()
            {
                pasajeroNombre = "pepe",
                posBaja = 1,
                posSube = 1
            };

            var dto = new ViajePost()
            {
                fechaInicio = DateTime.UtcNow,
                fechaFin = DateTime.UtcNow.AddHours(1),
                choferId = 1,
                tiempoEspera = 2,
                peajes = 30,
                posiciones = new PosicionPost[] { posicion }.ToList(),
                pasajeros = new PasajeroPost[] { pasajeroConocido, pasajeroDesconocido }.ToList()
            };

            var jsonValido = JsonConvert.SerializeObject(dto, Formatting.Indented);

            var fakeRepo = A.Fake<IRepositorioPasajeros>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(MapperFactory.GetInstance());
                with.Dependency<IRepositorioPasajeros>(fakeRepo);
                with.Dependency<IRepositorioPosiciones>(A.Fake<IRepositorioPosiciones>());
                with.Dependency<IRepositorioViajes>(A.Fake<IRepositorioViajes>());
                with.Module<ModuloViajes>();
            });
            var browser = new Browser(bootstrapper);

            // when
            var result = browser.Post(Rutas.Viajes, with =>
            {
                with.HttpRequest();
                with.Body(jsonValido);
            });

            //then
            Assert.Equal(EmptyJson, result.Body.AsString());
        }

        [Fact]
        public void deberia_guardar_usando_el_id_viaje_correcto_en_pasajeros()
        {
            // given
            var posicion = new PosicionPost()
            {
                id = 1,
                latitud = 1,
                longitud = 2,
                velocidad = 3,
                rumbo = 4,
                accuracy = 3,
                fecha = DateTime.UtcNow,
                versionLogica = 5
            };

            var pasajero = new PasajeroPost()
            {
                pasajeroNombre = "pepe",
                pasajeroId = 1,
                posBaja = 1,
                posSube = 1
            };

            var dto = new ViajePost()
            {
                fechaInicio = DateTime.UtcNow,
                fechaFin = DateTime.UtcNow.AddHours(1),
                choferId = 1,
                tiempoEspera = 2,
                peajes = 30,
                posiciones = new PosicionPost[] {posicion}.ToList(),
                pasajeros = new PasajeroPost[] {pasajero}.ToList()
            };

            var jsonValido = JsonConvert.SerializeObject(dto, Formatting.Indented);

            var idViajeEsperado = 1008;

            var fakeRepoViaje = A.Fake<IRepositorioViajes>();

            A.CallTo(() => fakeRepoViaje.guardarViaje(A<Viaje>._)).Returns(idViajeEsperado);

            var fakeRepo = A.Fake<IRepositorioPasajeros>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(MapperFactory.GetInstance());
                with.Dependency<IRepositorioPasajeros>(fakeRepo);
                with.Dependency<IRepositorioViajes>(fakeRepoViaje);
                with.Dependency<IRepositorioPosiciones>(A.Fake<IRepositorioPosiciones>());
                with.Module<ModuloViajes>();
            });
            var browser = new Browser(bootstrapper);

            // when
            var result = browser.Post(Rutas.Viajes, with =>
            {
                with.HttpRequest();
                with.Body(jsonValido);
            });

            Predicate<List<PasajeroViaje>> verificarPasajeros = delegate(List<PasajeroViaje> pasajeros)
            {
                Assert.Equal(1, pasajeros.Count);
                PasajeroViaje p = pasajeros[0];
                Assert.Equal(idViajeEsperado, p.IdViaje);
                return true;
            };

            //then
            A.CallTo(
                    () => fakeRepo.guardarPasajeros(A<List<PasajeroViaje>>.That.Matches(v => verificarPasajeros(v))))
                .MustHaveHappened();
        }

        [Fact]
        public void deberia_guardar_la_posicion_correcta_de_subida_y_bajada()
        {
            // given
            var posicionBajada = new PosicionPost()
            {
                id = 1,
                latitud = 1,
                longitud = 2,
                velocidad = 3,
                rumbo = 4,
                accuracy = 10,
                fecha = DateTime.UtcNow,
                versionLogica = 5
            };

            var posicionSubida = new PosicionPost()
            {
                id = 2,
                latitud = 1,
                longitud = 2,
                velocidad = 3,
                rumbo = 4,
                accuracy = 5,
                fecha = DateTime.UtcNow,
                versionLogica = 5
            };

            var pasajero = new PasajeroPost()
            {
                pasajeroNombre = "pepe",
                pasajeroId = 1,
                posBaja = posicionBajada.id,
                posSube = posicionSubida.id
            };

            var dto = new ViajePost()
            {
                fechaInicio = DateTime.UtcNow,
                fechaFin = DateTime.UtcNow.AddHours(1),
                choferId = 1,
                tiempoEspera = 2,
                peajes = 30,
                posiciones = new PosicionPost[] {posicionBajada, posicionSubida}.ToList(),
                pasajeros = new PasajeroPost[] {pasajero}.ToList()
            };

            var jsonValido = JsonConvert.SerializeObject(dto, Formatting.Indented);

            var idViajeEsperado = 1008;

            var fakeRepoViaje = A.Fake<IRepositorioViajes>();
            var fakeRepoPosiciones = A.Fake<IRepositorioPosiciones>();

            var posicionEsperadaSubida = 50;
            var posicionEsperadaBajada = 100;
            
            var listaFakeViajes = new List<Posicion>()
            {
                obtenerPosicion(posicionSubida, posicionEsperadaSubida),
                obtenerPosicion(posicionBajada, posicionEsperadaBajada)
            };

            A.CallTo(() => fakeRepoPosiciones.guardarPosiciones(A<List<Posicion>>._))
                .Returns(listaFakeViajes);
            A.CallTo(() => fakeRepoViaje.guardarViaje(A<Viaje>._)).Returns(idViajeEsperado);

            var fakeRepo = A.Fake<IRepositorioPasajeros>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(MapperFactory.GetInstance());
                with.Dependency<IRepositorioPasajeros>(fakeRepo);
                with.Dependency<IRepositorioViajes>(fakeRepoViaje);
                with.Dependency<IRepositorioPosiciones>(fakeRepoPosiciones);
                with.Module<ModuloViajes>();
            });
            var browser = new Browser(bootstrapper);

            // when
            var result = browser.Post(Rutas.Viajes, with =>
            {
                with.HttpRequest();
                with.Body(jsonValido);
            });

            Predicate<List<PasajeroViaje>> verificarPasajeros = delegate(List<PasajeroViaje> pasajeros)
            {
                Assert.Equal(1, pasajeros.Count);
                PasajeroViaje p = pasajeros[0];
                Assert.Equal(posicionBajada.id, p.IdPosicionBaja);
                Assert.Equal(posicionSubida.id, p.IdPosicionSube);
                return true;
            };

            //then
            A.CallTo(
                    () => fakeRepo.guardarPasajeros(A<List<PasajeroViaje>>.That.Matches(v => verificarPasajeros(v))))
                .MustHaveHappened();
        }

        private Posicion obtenerPosicion(PosicionPost posicionSubida, int posicionEsperadaSubida)
        {
            return new Posicion()
            {
                Id = posicionEsperadaSubida,
                IdViajeInterno = posicionSubida.id
            };
        }

        private SqlException MakeSqlException()
        {
            SqlException exception = null;
            try
            {
                SqlConnection conn = new SqlConnection(@"Data Source=.;Database=GUARANTEED_TO_FAIL;Connection Timeout=1");
                conn.Open();
            }
            catch (SqlException ex)
            {
                exception = ex;
            }
            return (exception);
        }
    }
}
