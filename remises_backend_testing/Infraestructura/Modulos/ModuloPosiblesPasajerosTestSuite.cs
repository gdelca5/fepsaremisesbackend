﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Nancy.Testing;
using remises_backend.AccesoDatos.Tablas;
using remises_backend.Infraestructura.Modulos;
using remises_backend.Infraestructura.Repositorios;
using Xunit;
using FakeItEasy;
using Nancy;
using remises_backend.Infraestructura.Dtos;
using remises_backend.Infraestructura.Mappers;

namespace remises_backend_testing.Infraestructura.Modulos
{
    public class ModuloPosiblesPasajerosTestSuite
    {
        [Fact]
        public void deberia_poder_ser_instanciado()
        {
            // given
            var fakeRepo = A.Fake<IRepositorio<PasajeroFepsa>>();
            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorio<PasajeroFepsa>>(fakeRepo);
                with.Module<ModuloPosiblesPasajeros>();
            });

            // when
            new Browser(bootstrapper);
        }

        [Fact]
        public void no_deberia_fallar_si_no_hay_datos_cargados()
        {
            // given
            var fakeRepo = A.Fake<IRepositorio<PasajeroFepsa>>();
            var lista = new List<PasajeroFepsa>();
            A.CallTo(() => fakeRepo.ObtenerTodos()).Returns(lista);

            var aFakeDto = A.Fake<ParametrosAppGet>();

            var fakeMapper = A.Fake<AutoMapper.IMapper>();

            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(fakeMapper);              
                with.Dependency<IRepositorio<PasajeroFepsa>>(fakeRepo);
                with.Module<ModuloPosiblesPasajeros>();
            });

            var browser = new Browser(bootstrapper);

            // when
            browser.Get(Rutas.PosiblesPasajeros, with =>
            {
                with.HttpRequest();
            });
        }

        [Fact]
        public void no_deberia_fallar_si_hay_datos_cargados()
        {
            // given
            var fakeRepo = A.Fake<IRepositorio<PasajeroFepsa>>();
            var pasajero = A.Fake<PasajeroFepsa>();
            var lista = new List<PasajeroFepsa>() {pasajero};
            A.CallTo(() => fakeRepo.ObtenerTodos()).Returns(lista);

            var aFakeDto = A.Fake<ParametrosAppGet>();
            var fakeMapper = A.Fake<AutoMapper.IMapper>();

            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorio<PasajeroFepsa>>(fakeRepo);
                with.Module<ModuloPosiblesPasajeros>();
            });

            var browser = new Browser(bootstrapper);

            // when
            browser.Get(Rutas.PosiblesPasajeros, with =>
            {
                with.HttpRequest();
            });
        }
        
        [Fact]
        public void deberia_llamar_al_mapper_dto()
        {
            // given
            var fakeRepo = A.Fake<IRepositorio<PasajeroFepsa>>();
            var fakeParametro = A.Fake<PasajeroFepsa>();
                           
            var lista = new List<PasajeroFepsa>() { fakeParametro };
            A.CallTo(() => fakeRepo.ObtenerTodos()).Returns(lista);
            
            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(fakeMapper);
                with.Dependency<IRepositorio<PasajeroFepsa>>(fakeRepo);
                with.Module<ModuloPosiblesPasajeros>();
            });
            var browser = new Browser(bootstrapper);
            
            // when
            var result = browser.Get(Rutas.PosiblesPasajeros, with =>
            {
                with.HttpRequest();
            });

            // then
            A.CallTo(() => fakeMapper.Map<PasajeroFepsa, PosiblePasajeroInner>(A<PasajeroFepsa>._)).MustHaveHappened();
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        }
        
        [Fact]
        public void deberia_devolver_los_datos_esperados_correctamente()
        {
            // given
            var fakeRepo = A.Fake<IRepositorio<PasajeroFepsa>>();
            var version = new VersionPasajero()
            {
                Id = 1,
                detalle = "algun id de pasajeros"
            };
            
            var parametro = new PasajeroFepsa()
            {
                Id = version.Id,
                NombreApellido = "Pepe",
                Legajo = "1",
                IdVersion = 1
            };
                           
            var lista = new List<PasajeroFepsa>() { parametro };
            A.CallTo(() => fakeRepo.ObtenerTodos()).Returns(lista);
            
            var fakeMapper = A.Fake<AutoMapper.IMapper>();
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Dependency<AutoMapper.IMapper>(MapperFactory.GetInstance());
                with.Dependency<IRepositorio<PasajeroFepsa>>(fakeRepo);
                with.Module<ModuloPosiblesPasajeros>();
            });
            var browser = new Browser(bootstrapper);
            
            // when
            var result = browser.Get(Rutas.PosiblesPasajeros, with =>
            {
                with.HttpRequest();
            });

            // then
            var actual = result.Body.DeserializeJson<PosiblePasajero>();
            
            Assert.Equal(actual.pasajeros.Count, 1);
            
            Assert.Equal(parametro.Id.ToString(), actual.pasajeros[0].pasajeroId);
            Assert.Equal(parametro.NombreApellido, actual.pasajeros[0].nombre);
            Assert.Equal(parametro.IdVersion, actual.version);
        }        
    }
}