# FEPSA Remises Documentaci�n

## Indice:

* Como comenzar a usar la app
* Pruebas locales para desarrollo usando Docker
* Esquema de base de datos
* API
* Descarga de binarios


### Como comenzar a usar la app

Para poder comenzar a usar la app se necesita que se carguen los parametros de configuraci�n iniciales. Los mismos pueden hacerse desde la api de swager de manera muy sencilla ya que traen valores de ejemplo (ver el endpoint POST /app-info)

### Pruebas locales para desarrollo usando Docker

Para poder levantar un entorno de pruebas con el backend y la base de datos ya creada se puede hacer lo siguiente:

* Instalar [Docker CE](https://store.docker.com/editions/community/docker-ce-desktop-windows)
* Compilar o descargar los archivos pre compilados. Los mismos deben estar en */remises_backend/bin/Debug/*
* Ejecutar el siguente comando en la raiz del repositorio

```
docker-compose up
```

El mismo comando levanta en el port 127.0.0.1:3306 una base de datos con el esquema ya creado utilizando el archivo *create_db_mysql.sql* y levanta el backend en el port 127.0.0.1:8888.

#### Apagar todo

```
docker-compose down
```

####�Apagar solo el backend pero dejar la base de datos

```
docker-compose stop backend
```

####�Apagar solo la base de datos pero dejar el backend

```
docker-compose stop db
```


###�Esquema de base de datos

La base de datos consta de 8 tablas:

* parametros_logica: Contiene los parametros de funcionamiento de la app
* versiones_pasajeros: Contiene las versiones de las listas de los pasajeros disponibles que busca la app
* choferes: Contiene los datos de los choferes de las remiseras
* remiseras: Contiene los codigos de activacion de cada remisera disponible
* viajes: Contiene los viajes
* posiciones: Contiene las posiciones de cada viaje
* pasajeros_fepsa: Contiene los datos de los posibles pasajeros


![Diagrama ER](documentacion/diagrama_er.png?raw=true "Diagrama Entidad Relaci�n")

### API

Para poder ver los endpoints disponibles de la API del backend, se puede acceder al swagger e interactuar con las APIs ah� mismo.

Una vez desplegado el backend, las swagger estan disponibles con una llamada al browser donde este atendiendo ej: [remises.fepsa.com.ar](http://ec2-54-69-12-250.us-west-2.compute.amazonaws.com/)

![Swagger API](documentacion/swagger-preview.png?raw=true "Swagger API Preview")

###�Descarga de binarios

Hay binarios pre compilados en [Downloads](https://bitbucket.org/fullexpress/fepsaremisesbackend/downloads/). Los mismos son compatibles tanto con .NET 4.6 como con mono 5.4.1.7.