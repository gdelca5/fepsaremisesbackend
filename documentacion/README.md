
## Como levantar el swagger ui ?

Ejecutar los siguientes comandos para levantar el swagger editor localmente usando docker en el port 8080

```
docker pull swaggerapi/swagger-ui
docker run -p 8080:8080 -e SWAGGER_JSON=/foo/api.swagger.yaml -v $PWD:/foo swaggerapi/swagger-ui
```


